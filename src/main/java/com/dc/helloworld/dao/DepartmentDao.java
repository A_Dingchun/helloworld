package com.dc.helloworld.dao;


import com.dc.helloworld.po.Department;
import org.junit.Test;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
public class DepartmentDao {
    private static Map<Integer, Department> departments = null;

    static {
        departments = new HashMap<>();
        departments.put(101,new Department(101,"行政部"));
        departments.put(102,new Department(102,"营销部"));
        departments.put(103,new Department(103,"人力资源部"));
        departments.put(104,new Department(104,"财务部"));
        departments.put(105,new Department(105,"技术部"));

    }

    //获取所有部门信息
    public Collection<Department> getDepartments(){
        return departments.values();
    }
    //根据id获取部门
    public Department getDepartmentById(Integer id){
        return departments.get(id);
    }


    @Test
    public void test(){
        Map<Integer, Department> departments = new HashMap<>();
        departments.put(101,new Department(101,"行政部"));
        departments.put(102,new Department(102,"营销部"));
        departments.put(103,new Department(103,"人力资源部"));
        departments.put(104,new Department(104,"财务部"));
        departments.put(105,new Department(105,"技术部"));
        departments.forEach((k,v)->{
            System.out.println(k);
            System.out.println(v.getDepartmentName());
        });
    }






}
