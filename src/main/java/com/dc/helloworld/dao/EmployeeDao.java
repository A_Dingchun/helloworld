package com.dc.helloworld.dao;


import com.dc.helloworld.po.Department;
import com.dc.helloworld.po.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
public class EmployeeDao {

    private static Map<Integer, Employee> employees = null;

    @Autowired
    private DepartmentDao departmentDao;

    static {
        employees = new HashMap<>();
        employees.put(1001,new Employee(1001,"A",1,"12511@qq.com",new Department(101,"行政部")));
        employees.put(1002,new Employee(1002,"B",0,"22511@qq.com",new Department(102,"营销部")));
        employees.put(1003,new Employee(1003,"C",1,"32511@qq.com",new Department(103,"人力资源部")));
        employees.put(1004,new Employee(1004,"D",0,"42511@qq.com",new Department(104,"财务部")));
        employees.put(1005,new Employee(1005,"E",1,"52511@qq.com",new Department(105,"技术部")));
    }


    private Integer initId = 1006;//自增主键

    //新增或修改员工
    public void save(Employee employee){
        if (employee.getId()==null){
            employee.setId(initId++);
        }
        //前端只给部门id，需要自己封装Department对象关联
        Department department = departmentDao.getDepartmentById(employee.getDepartment().getId());
        employee.setDepartment(department);
        employees.put(employee.getId(),employee);//Map保存key重复的对象会覆盖value 所以可以修改和新增
    }

    //查询全部员工信息
    public Collection<Employee> getAllEmployees() {
        return employees.values();
    }

    //通过id查询员工
    public Employee getEmployeeByID(Integer id) {
        return employees.get(id);
    }

    //删除
    public void delete(Integer id){
        employees.remove(id);
    }

}
