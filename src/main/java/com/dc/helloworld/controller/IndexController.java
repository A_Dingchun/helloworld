package com.dc.helloworld.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {

    @RequestMapping("/index")
    public String toIndex(Model model){
        model.addAttribute("msg","hello,springboot!");
        //classpath:/templates/index.html
        return "index";
    }



}
