package com.dc.helloworld.pojo;


import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Email;
import java.util.Date;
import java.util.List;
import java.util.Map;

//@ConfigurationProperties是springboot提供读取配置文件的一个注解

@Component //注册bean到容器中
@ConfigurationProperties(prefix = "person")//读取配置文件的一个注解
@Validated //数据校验
@Data
public class Person {
    private String name;
    private Integer age;
    private Boolean happy;
    private Date birth;
    private Map<String,Object> maps;
    private List<Object> lists;
    private Dog dog;

    @Email(message = "不是邮件格式")
    private String email;


    //有参无参构造、get、set方法、toString()方法
}
