package com.dc.helloworld.pojo;


import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;



@Component //注册bean
@PropertySource(value = "classpath:person.properties")//加载指定的配置文件；
@Data
public class User {
    //直接使用@value
    @Value("${user.name}") //从配置文件中取值
    private String name;
    @Value("#{5*3}")  // #{SPEL} Spring表达式
    private int age;
    @Value("男")  // 字面量
    private String sex;

}