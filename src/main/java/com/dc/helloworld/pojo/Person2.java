package com.dc.helloworld.pojo;


import lombok.Data;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Value;

//加载指定的配置文件
@PropertySource(value = "classpath:person.properties")
@Component //注册bean
@Data
public class Person2 {

    @Value("${name}")
    private String name;

}