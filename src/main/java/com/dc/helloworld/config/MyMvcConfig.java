package com.dc.helloworld.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


//因为类型要求为WebMvcConfigurer，所以我们实现其接口
//可以使用自定义类扩展MVC的功能
@Configuration
public class MyMvcConfig implements WebMvcConfigurer {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {

        //浏览器发送/login.html ， 就会跳转到login页面；
        registry.addViewController("/").setViewName("login");
        registry.addViewController("/login.html").setViewName("login");
        registry.addViewController("/main.html").setViewName("dashboard");


    }

    //国际化方法一：给容器添加一个自定义国际化解析器 localeResolver方法名固定
    @Bean
    public MyLocaleResolver localeResolver(){
        return new MyLocaleResolver();
    }


    //自定义拦截方法
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //拦截不包括。。。的所有请求  /**
        registry.addInterceptor(new LoginHandlerInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns("/login.html", "/", "/user/login",
                        "/asserts/**","/favicon.ico");

    }



    //国际化方法二：自定义国际化解析方式
    //通过在请求中添加 lang 来指定当前环境信息。这个指定只需要一次即可，
    //也就是说，在 session 不变的情况下，下次请求可以不必带上 lang 参数，服务端已经知道当前的环境信息了
/*
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        LocaleChangeInterceptor interceptor = new LocaleChangeInterceptor();
        interceptor.setParamName("lang");
        registry.addInterceptor(interceptor);
    }
    @Bean
    LocaleResolver localeResolver() {
        SessionLocaleResolver localeResolver = new SessionLocaleResolver();
        localeResolver.setDefaultLocale(Locale.SIMPLIFIED_CHINESE);
        return localeResolver;
    }
*/


}