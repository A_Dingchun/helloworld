package com.dc.helloworld.po;

import com.dc.helloworld.po.Department;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class Employee {
    private Integer id;
    private String name;
    private Integer gender;// 0女  1男
    private String email;
    private Department department;
    private Date birthday;

    public Employee(Integer id, String name, Integer gender, String email, Department department) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.email = email;
        this.department = department;
        this.birthday = new Date();
    }
}
