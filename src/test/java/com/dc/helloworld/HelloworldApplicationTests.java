package com.dc.helloworld;

import com.dc.helloworld.pojo.Dog;
import com.dc.helloworld.pojo.Person;
import com.dc.helloworld.pojo.Person2;
import com.dc.helloworld.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class HelloworldApplicationTests {

    @Autowired
    Dog dog;

    @Autowired
    Person person;

    @Autowired
    Person2 person2;

    @Autowired
    User user;

    @Test
    void contextLoads() {
        System.out.println(dog);
        System.out.println(person);
        System.out.println(person2);

        System.out.println(user);
    }

}
